__author__ = 'jseller'

import os, csv
from collections import Counter
from itertools import combinations
from random import shuffle

# eight people in a camp, 2 per canoe, 3 possible locations or sit
PEOPLE = ['jon', 'mike', 'pj',  'dk', 'phil', 'james', 'surly', 'clayton']
POOLS = ['marion','bogan','camp']
GUIDES = ['mike','bruno','dave']

'''
Guides rotate on pools, so match up canoes and locations
Camp is usually four days, 2 sessions per day
The total sessions can be limited, and potentially the people could be many. 
Since the guides and pools are fixed, the fairest would probably be to pick the next 
unique pairing from a random selection of all the possible
'''
SESSIONS = 6
'''
# for length of time the trip, create the guide/pool set of timeslots
# distribute the group evenly along the timeslots
#create all the fisher pairings and order sucessively along that list
#radomize the order of the fishers and create pairings

Each fisher wants to fish with their buddies the same amount and everyone wants to fish the pools equally

The amount of partner combinations could be more than the sessions available.
add 'sitting' to pools 
sitting fishers (3 pools only 6 fishers)

'''
shuffle(PEOPLE)

#create all the unique fisher pairings and order sucessively along that list
# first gets first pick then placed at the end of the list
def get_pairings(group):
    pairs = []
    while len(group) > 0:
        first = group.pop()
        for g in group:
            pairs.append((first,g))
    return pairs

fishers = PEOPLE
num_fishers = len(fishers)
pairings = get_pairings(list(fishers))
print(str(pairings))

# total combinations should be: n*(n-1)/2
total_combos = (num_fishers * (num_fishers-1)) /2
assert len(pairings) == total_combos

# for all the pick lists, get the pairs set for each pool
def in_pairs(f, pairs):
    for s in pairs:
        if f == s[0] or f == s[1]:
            return True
    return False

def find_pair(fisher, session_pairings):
    for pair in pairings:
        if (fisher == pair[0] or fisher == pair[1]) and (not in_pairs(pair[0], session_pairings) and not in_pairs(pair[1], session_pairings)):
            return pair

def get_pick_list(fisher_list):
    pick_lists = []
    fish_pick = fisher_list
    pick_lists.append(fish_pick)
    for x in range(SESSIONS-1):
        new_pick_idx = (x + 1) % len(fish_pick)
        new_pick = fish_pick[new_pick_idx:] + fish_pick[:new_pick_idx]
        pick_lists.append(new_pick)
    return pick_lists

the_pick_lists = get_pick_list(fishers) 
print(str(the_pick_lists))

trip_pairings = []
for session in the_pick_lists:
    session_pairings = []
    for fisher in session:
        pair = find_pair(fisher, session_pairings)
        if pair:
            pairings.remove(pair)
            session_pairings.append(pair)
    trip_pairings.append(session_pairings)   
print str(trip_pairings)

'''
the camp only has 3 pools, whoever draws last sits. Fishers only want to sit the same amount
Assign fishers to pool/pools, or sit

could be more sitters than fishers, and all the fishers 
would all have a chance fishing before someone goes twice.

Take the fishers that fished out of the pick of the next, until everyones done

'''



