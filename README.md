# README #

Projects for fun

### Dart scores ###

* What dart sequences can you finish a game on? The player has to end on a double score.

### Lotto Stats ###

* What are the most commonly drawn numbers? There seems to be a high number of prime numbers in the results, but I have no idea why.

### Fishing spots ###

* For a number of people fishing at a camp, there are a number of locations and a number of guides. How do you group the people together so they have equal time on the locations with the guides?
* This is really a problem for a constraint based language, or help from a similar library
* http://labix.org/python-constraint