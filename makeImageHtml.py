import os, time, sys, image


#go though directory,
#change file names with spaces to proper names
#output file names with html images

#myDirectory = "C:\Users\Owner\Documents\Aptana Studio Workspace\wdfund\html/golf/wdmgt_photos_2010/"

myDirectory = "C:\Users\Owner\Documents\Aptana Studio Workspace\wdfund/html/golf/test/"


def resizeImage(imageFile):
    im1 = Image.open(imageFile)
    # adjust width and height to your needs
    width = 250
    height = 210
    im5 = im1.resize((width, height), Image.ANTIALIAS)    
    ext = ".jpg"
    newname = imageFile+"small"
    im5.save(newname + ext)


theFiles = []

def listFiles(dname):
    theList = []
    # make sure no files with 'h_'
    for file in os.listdir(dname):
        if not file.startswith('h_'):
            theList.append(file)
    return theList

print "list files in out.text"+myDirectory

dir = myDirectory
if os.path.exists(dir):
    theFiles = listFiles(dir)
else:
    print "not a dir"

print theFiles

def getNewFileNames():
    newList = []
    for file in theFiles:
        filename = file
        fullpath = myDirectory
        fullpath +=file
        print fullpath
        filename = filename.replace(" ","_")
        newfullpath = myDirectory+filename
        print newfullpath
        newList.append(filename)
        if fullpath != newfullpath:
            os.rename(fullpath,newfullpath)
        resizeImage(newfullpath)
    return newList

print theFiles

fileList = getNewFileNames()

html ="<div>"

for file in fileList:
    html += "<img src=\"wdmgt_photos_2010/"
    html +=file
    html +="\"></img>"

html +="</div>"

outFile = open('out.text','w')
outFile.write(html)
outFile.close()
