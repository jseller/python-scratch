'''
how many score in darts can you dart out on?
must dart out on a double
'''
scores = [x+1 for x in xrange(20)]
double_scores = [x * 2 for x in scores]
triple_scores = [x * 3 for x in scores]
possible_scores = set(scores).union(double_scores).union(triple_scores)

# could use a set for ordering the resulting scores, but instead 
# using the keys from the dict since it will end up being the same unique values
double_out_scores = {}
def add_to_double_out_scores(dart, second_dart=0, third_dart=0):
    idx = dart + second_dart + third_dart
    res = None
    if second_dart > 0 and third_dart > 0:
        res = [third_dart, second_dart, dart]
    elif second_dart > 0:
        res = [second_dart, dart]
    else:
        res = [dart]
    if res:
        if double_out_scores.get(idx):
            double_out_scores[idx].append(res)
        else:
            double_out_scores[idx] = [res]

for first_dart in double_scores:
    add_to_double_out_scores(first_dart)
    for second_dart in possible_scores:
        add_to_double_out_scores(first_dart, second_dart)
        for third_dart in possible_scores:
            add_to_double_out_scores(first_dart, second_dart, third_dart)

print str(double_out_scores)
dict_ids = [x for x in reversed(double_out_scores.keys())]
print dict_ids
