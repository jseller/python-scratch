__author__ = 'jseller'

import os, csv
from collections import Counter
from itertools import combinations
from random import shuffle

# eight people in a camp, 2 per canoe, 3 possible locations or sit
people = ['jon', 'mike', 'pj',  'dk', 'phil', 'james', 'surly', 'clayton']
locations = ['marion','bogan','camp']
guides = ['mike','bruno','dave']

'''
Guides rotate on pools, so match up canoes and locations
Camp is usually four days, 2 sessions per day
The total sessions can be limited, and potentially the people could be many. 
Since the guides and pools are fixed, the fairest would probably be to pick the next 
unique pairing from a random selection of all the possible
'''
total_sessions = 12

class Session(object):
    def __init__(self, guide, location, fisher1 = None, fisher2 = None):
        self.guide = guide
        self.location = location
        self.fisher1 = fisher1
        self.fisher2 = fisher2

    def add_fisher(self, fisher):
        if fisher and fisher1 is None:
            self.fisher1 = fisher
        elif fisher and self.fisher2 is None:
            self.fisher2 = fisher
        else:
            raise

    def __str__(self):
        return 'hi' #"%s %s %s %s" % (self.guide,self.location,self.fisher1, self.fisher2)

class Fisher(object):
    def __init__(self):
        self.pools = []
        self.guides = []
        self.partners = []
        self.fished = 0

    def fish(self, pool, partner, guide):
        self.pools.append(pool)
        self.partners.append(partner)
        self.guides.append(guide)
        self.fished = self.fished + 1

all_combinations = [",".join(map(str,comb)) for comb in combinations(people, 2)]
print "all combos"+str(len(all_combinations))
days = []

def mix_it_up(a_list):
    length = len(a_list)
    nums = [int(x) for x in xrange(length)]
    shuffle(nums)
    new_list = [a_list[num] for num in nums]
    return new_list

def get_session(the_list):
    if len(the_list) == 0:
        return
    groups = []
    remaing = []
    for fishers in the_list:
        fisher1 = fishers.split(',')[0]
        fisher2 = fishers.split(',')[1]
        if len(groups) < 6 and fisher1 not in groups and fisher2 not in groups:
            groups.append(fisher1)
            groups.append(fisher2)
        else:
            remaing.append(fishers)
    return groups, remaing

all_combinations = mix_it_up(all_combinations)
print str(all_combinations)
groups, remaining = get_session(all_combinations)

fishing_spots = []
for i in xrange(total_sessions):
    guide = guides[i % len(guides)]
    #every session move the guides up a pool
    day = (i / 3)
    location = locations[(day + i % 3) % len(locations)] + str(day)
    idx = (i % 3) * 2        
    fishing_spots.append(Session(guide,location,groups[idx],groups[idx+1]))
    if (i % 3) == 2:
        groups, remaining = get_session(remaining)

print str([a.guide+" "+a.location+" "+a.fisher1+" "+a.fisher2 for a in fishing_spots])

remaining_people = []
for fishers in remaining:
    fisher1 = fishers.split(',')[0]
    fisher2 = fishers.split(',')[1]
    remaining_people.append(fisher1)
    remaining_people.append(fisher2)

data = Counter(remaining_people)
print data.most_common(len(people))
