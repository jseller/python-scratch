#!/usr/bin/env python

import unittest
#from calculator import Calculator

class Calculator(object):
    def add(self, x, y):
        pass
 
class TddInPythonExample(unittest.TestCase):
    def test_calculator_add_method_returns_correct_result(self):
        calc = Calculator()
        res = calc.add(3,4)
        self.assertEquals(res,7)
        self.assertEquals(calc.add(5,5),10)
        self.assertEquals(calc.add(5675,53211),58886)


