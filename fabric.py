# this is a handy script to automate operation steps for deploying new versions

from fabric.api import *

home_dir = '/opt/sphere/web'
jars = ['build/dist/someapp.jar']
configs = ['build/dist/config.yml']
scripts = ['mcweb.sh']

print env.hosts

def deploy():
    stop_service()
    create_deploy_dir()
    deploy_jars()
    deploy_configs()
    start_service()

def start_service():
    run('nohup ' + home_dir + '/webapp.sh start logfile &> /dev/null &', pty=False)
    
def stop_service():
    cmd = home_dir+ '/mcweb.sh stop'
    run(cmd)

def create_deploy_dir():
    run('mkdir -p ' + home_dir)

def deploy_jars():
    for jar in jars:
        put(jar, home_dir)

def deploy_configs():
    for config in configs:
        put(config, home_dir)

def deploy_scripts():
    for script in scripts:
        put('scripts/' + script, home_dir)
        run('chmod +x ' + home_dir + '/' + script)
        
def clean():
    stop_service()
    run('rm -rf ' + home_dir)
